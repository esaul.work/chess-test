<?php
function dump($arr)
{
    echo '<pre>', print_r($arr, 1), '</pre>';
}

$search = new testChess();
$haystack = $search->getTestCases();
$result = $search->run();
dump('Needle = ' . testChess::NEEDLE);
dump($result);

class testChess
{

    public const NEEDLE = 11;
    private $testCases = [];
    private $debug = false;

    private function init()
    {
        $this->testCases = [
            [-12, -6, -1, 0, 1, 4, 7, 8, 15, 22],
            [5, 7, 19],
            [2],
            [],
            [-5, 0, 3, 5],
            [-12, -6, -1, 0, 4, 6, 9, 11, 23, 55, 89, 34, 122]
        ];
    }

    public function __construct()
    {
        $this->init();
    }

    /**
     * @return array
     */
    public function getTestCases()
    {
        return $this->testCases;
    }

    /**
     * @param int $needle
     * @return array
     */
    public function run(int $needle = self::NEEDLE): array
    {
        $result = [];
        foreach ($this->testCases as $testCase) {
            $left = 0;
            $right = count($testCase) - 1;
            $result[] = $this->search($needle, $testCase, $left, $right);
        }
        return $result;
    }

    /**
     * @param int $needle
     * @param array $array
     * @param int $left
     * @param int $right
     * @return mixed
     */
    public function search(int $needle, array &$array, int $left, int $right)
    {
        if ($needle <= $array[$left]) {
            return $array[$left];
        }
        if(
            $needle >= $array[$right]
            && $right === count($array) - 1
        ) {
            return $array[$right];
        }
        $index = round(($left + $right) / 2);
        $diff = $right - $left;
        if ($diff > 1) {
            // Ищем середину массива
            if ($array[$index] >= $needle) {
                // Берем левую часть
                $right = $index;
            } else {
                // Берем правую часть
                $left = $index;
            }
            // Продолжаем половинное деление
            return $this->search($needle, $array, $left, $right);
        } elseif (count($array) === 1) {
            // Если элемент 1 в массиве - его и вернем
            return $array[0];
        } else {
            // найдем ближайший к заданному числу
            return ($needle - $array[$left] > $array[$right] - $needle) ? $array[$right] : $array[$left];
        }
    }

    /**
     * @param mixed $data
     */
    public function debug($data) {
        if ($this->debug) {
            dump($data);
        }
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <script>
        function _search(digit, haystack, left, right) {
            if (digit <= haystack[left]) {
                return haystack[left];
            }
            if (
                haystack[right] >= digit
                && right === haystack.length
            ) {
                return haystack[right];
            }
            let diff = right - left;
            if (diff > 1) {
                let index = Math.ceil((left + right) / 2);
                if (haystack[index] >= digit) {
                    right = index;
                } else {
                    left = index;
                }
                return _search(digit, haystack, left, right);
            } else if (haystack.length === 1) {
                return haystack[0];
            } else {
                return (digit - haystack[left] > haystack[right] - digit) ? haystack[right] : haystack[left];
            }
        }

        window.onload = () => {
            let testCases = [
                [-12, -6, -1, 0, 1, 4, 7, 8, 15, 22],
                [5, 7, 19],
                [2],
                [],
                [-5, 0, 3, 5],
                [-14, -12, -6, -1, 0, 4, 6, 9, 11, 23, 55, 89, 34, 122]
            ];
            let digit = -13;
            let left = 0;
            let right = 0;
            let result = [];
            document.getElementById('digit').innerHTML = 'find nearest to ' + digit;
            document.getElementById('input').innerHTML = 'input: <br>';
            testCases.forEach((testCase) => {
                left = 0;
                right = testCase.length - 1;
                result.push(_search(digit, testCase, left, right));
                document.getElementById('input').innerHTML += '[' + testCase + ']<br>';
            })
            document.getElementById('result').innerHTML = 'result: ' + result;
        }
    </script>
</head>
<body>
    <div id="input">
    </div>
    <div id="digit">
    </div>
    <div id="result">
    </div>
</body>
</html>
